#include <iostream>
#include <cstdlib>
#include <cstring>
#include <sstream>
using namespace std;

void reverse_array(int* M, int N, int start)
{
	for (int i = 0; i<(N + 1) / 2; i++)  swap(M[i+start], M[N + start - 1 - i]);
}

void error_message()
{
	cout << "An error occured while reading input data"; 
	exit(0); 
}

int main()
{
	int N, k = 0;
	char symbol;
	bool correct;
	cin >> N;
	if (N < 0) error_message();
	int* A = new int[N];
	for (int i = 0;; i++) 
	{
		cin >> A[i];
		symbol = cin.get();
		if (symbol == '\n' && i == N - 1) break;
		else if ((symbol == '\n') || (i > N - 2)) error_message();
	}
	cin >> k;

	reverse_array(A, N - k, 0);
	reverse_array(A, k, N - k);
	reverse_array(A, N, 0);

for (int i = 0; i<N; i++) cout << A[i] << " ";

delete A;

system("pause");
}
